# == Schema Information
#
# Table name: searches
#
#  id          :integer         primary key
#  name        :string(255)
#  game        :string(255)
#  max_member  :integer
#  min_age     :integer
#  max_age     :integer
#  city        :string(255)
#  play_online :boolean
#  created_at  :datetime
#  updated_at  :datetime
#  monday      :boolean
#  tuesday     :boolean
#  wednesday   :boolean
#  thursday    :boolean
#  friday      :boolean
#  saturday    :boolean
#  sunday      :boolean
#

require 'test_helper'

class SearchTest < ActiveSupport::TestCase
  def test_should_be_valid
    assert Search.new.valid?
  end
end
