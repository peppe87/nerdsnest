class AddParametersToSearch < ActiveRecord::Migration
  def change
     add_column :searches, :monday, :boolean
     add_column :searches, :tuesday, :boolean
     add_column :searches, :wednesday, :boolean
     add_column :searches, :thursday, :boolean
     add_column :searches, :friday, :boolean
     add_column :searches, :saturday, :boolean
     add_column :searches, :sunday, :boolean
     add_column :searches, :age, :integer
  end
end
