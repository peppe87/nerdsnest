class CreateGroups < ActiveRecord::Migration
  def up
    create_table :groups do |t|
      t.string :name
      t.string :game
      t.integer :master_id
      t.integer :max_member
      t.integer :max_age
      t.integer :min_age
      t.string :city
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wednesday
      t.boolean :thursday
      t.boolean :friday
      t.boolean :saturday
      t.boolean :sunday
      t.boolean :play_online
      t.timestamps
    end
  end

  def down
  end
end
