class CreateSearches < ActiveRecord::Migration
  def self.up
    create_table :searches do |t|
      t.string :name
      t.string :game
      t.integer :max_member
      t.integer :min_age
      t.integer :max_age
      t.string :city
      t.boolean :play_online
      t.timestamps
    end
  end

  def self.down
    drop_table :searches
  end
end
