NerdsNest::Application.routes.draw do

  get "dices/index"

  get "dices/create"

  get "dice/index"

  get "dice/create"

  resources :users do
    resources :messages do
        collection do
            post :delete_selected
        end
    end
  end

  resources :groups, :messages, :groupships, :searches, :rooms, :dices

  # standard routes for sessions ([resource name]_path), with actions limited to new, create and destroy (no needs for editing or showing a session)
  resources :sessions, only: [:new, :create, :destroy]
  
  # standard routes for microposts, with actions limited to create and destroy
  resources :microposts, only: [:create, :destroy]

  root to: 'pages#home'
  
  match '/signup',  to: 'users#new'

  match '/signin', to: 'sessions#new'
  match '/signout', to: 'sessions#destroy', via: :delete # invoked by HTTP DELETE request

  match '/contact', to: 'pages#contact'
  
  match '/about', to: 'pages#about'
  
  match '/simple_search', to: 'pages#simple_search'
  
  match '/party/:id', to: 'rooms#party'
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
