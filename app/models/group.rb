# == Schema Information
#
# Table name: groups
#
#  id          :integer         not null, primary key
#  name        :string(255)
#  game        :string(255)
#  master_id   :integer
#  max_member  :integer
#  max_age     :integer
#  min_age     :integer
#  city        :string(255)
#  monday      :boolean
#  tuesday     :boolean
#  wednesday   :boolean
#  thursday    :boolean
#  friday      :boolean
#  saturday    :boolean
#  sunday      :boolean
#  play_online :boolean
#  created_at  :datetime
#  updated_at  :datetime
#

class Group < ActiveRecord::Base
  attr_accessible :name, :game, :master_id, :max_age, :min_age, :max_member, :city, :monday, 
                                    :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday, :play_online

  #groups functionalities
  has_many :groupships, dependent: :destroy
  has_many :users, :through => :groupships
  
  # micropost-related functionalites
  has_many :microposts, dependent: :destroy
  
  # name cannot be an empty strings or longer than 50 characters...
  validates :name, presence:true, length: { maximum: 50 }
  validates :game, presence:true, length: { maximum: 50 }
  validates :master_id, presence:true
  validates :max_age, presence:true, :numericality => {:less_than_or_equal_to => 100}
  validates :min_age, presence:true, :numericality => {:greater_than => 0, :less_than_or_equal_to => :max_age}
  validates :max_member, presence:true, :numericality => {:greater_than => 0}
  
    
  def feed
    # This is preliminary.
    Micropost.where("group_id = ?", id)
  end
  
  def self.search(search)
    if search
      where('groups.name LIKE ?', "%#{search}%")
    else
      scoped
    end
  end
  
  
end