# == Schema Information
#
# Table name: users
#
#  id              :integer         not null, primary key
#  name            :string(255)
#  email           :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  password_digest :string(255)
#  remember_token  :string(255)
#  admin           :boolean         default(FALSE)
#  birthdate       :date
#

class User < ActiveRecord::Base
  # admin MUST not appear in the att_accessible list: we don't want that external users are able to create a new administrator by acting on the URI...
  attr_accessible :name, :email, :password, :password_confirmation, :birthdate
  
  has_secure_password
  
  
  # messages functionalities
  has_private_messages
  
  # groups functionalities
  has_many :groupships
  has_many :groups, :through => :groupships
  
  # micropost-related functionalites
  has_many :microposts, dependent: :destroy
  
  # callback: assure that the mail uniqueness in the DB works with uppercase and downcase email addresses
  before_save { |user| user.email = email.downcase }
  
  before_save :create_remember_token
  
  # name cannot be an empty strings or longer than 50 characters...
  validates :name, presence:true, length: { maximum: 50 }
  
  # email cannot be an empty string and must have a specific format
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  
  #validazione della data di nascita
  validates_inclusion_of :birthdate, :in => Date.civil(1900,1,1)..Date.today,
  :message => "must be between 1900 and now"
  
  # password must be long 6 chars, at least
  validates :password, length: { minimum: 6 }
  
  # password_confirmation cannot be an empty string
  validates :password_confirmation, presence:true
  
  def feed
    # This is preliminary.
    Micropost.where("user_id = ?", id)
  end
  
  def joined?(group)
    groupships.find_by_group_id(group.id)
  end

  def join!(group)
    groupships.create!(group_id: group.id)
  end

  def leave!(group)
    groupships.find_by_group_id(group.id).destroy
  end
  
  def delete!(group)
    groups.find_by_id(group.id).destroy
  end
  
  def self.search(search)
    if search
      where('users.name LIKE ?', "%#{search}%")
    else
      scoped
    end
  end
  
  
private

    def create_remember_token
      # in Ruby 1.8.7 you should use SecureRandom.hex here instead
      self.remember_token = SecureRandom.urlsafe_base64
    end
end
