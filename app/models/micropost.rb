# == Schema Information
#
# Table name: microposts
#
#  id         :integer         primary key
#  content    :string(255)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  group_id   :integer
#  sender_id  :integer
#

class Micropost < ActiveRecord::Base
  
  # note: to create a new micropost, we need to write user.micropost.create instead of Micropost.create. We want that a micropost belong to a specific user.
  
  attr_accessible :content
  belongs_to :group
  belongs_to :user
  
  validates :sender_id, presence: true
  validates :content, presence: true, length: { maximum: 160 }
  
  default_scope order: 'microposts.created_at DESC'
  
end
