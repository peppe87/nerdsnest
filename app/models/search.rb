# == Schema Information
#
# Table name: searches
#
#  id          :integer         primary key
#  name        :string(255)
#  game        :string(255)
#  max_member  :integer
#  min_age     :integer
#  max_age     :integer
#  city        :string(255)
#  play_online :boolean
#  created_at  :datetime
#  updated_at  :datetime
#  monday      :boolean
#  tuesday     :boolean
#  wednesday   :boolean
#  thursday    :boolean
#  friday      :boolean
#  saturday    :boolean
#  sunday      :boolean
#

class Search < ActiveRecord::Base
  attr_accessible :name, :game, :max_member, :min_age, :max_age, :city, :play_online, :monday, 
                                    :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday
  
  def groups
    @groups ||= find_groups
  end
  
  private
  
  def find_groups
    Group.find(:all, :conditions => conditions)
  end
  
  def name_conditions
    ["groups.name LIKE ?", "%#{name}%"] unless name.blank?
  end
  
  def game_conditions
    ["groups.game LIKE ?", "%#{game}%"] unless game.blank?
  end
  
  def city_conditions
    ["groups.city LIKE ?", "%#{city}%"] unless city.blank?
  end
  
  def min_age_conditions
    ["groups.min_age >= ?", min_age] unless min_age.blank?
  end
  
  def max_age_conditions
    ["groups.max_age <= ?", max_age] unless max_age.blank?
  end
  
  def max_member_conditions
    ["groups.max_member = ?", max_member] unless max_member.blank?
  end
  
  def play_online_conditions
    ["groups.play_online = ?", play_online] unless play_online.nil?
  end
  
  def monday_conditions
    ["groups.monday = ?", monday] unless monday.nil?
  end
  
  def tuesday_conditions
    ["groups.tuesday = ?", tuesday] unless tuesday.nil?
  end
  
  def wednesday_conditions
    ["groups.wednesday = ?", wednesday] unless wednesday.nil?
  end
  
  def thursday_conditions
    ["groups.thursday = ?", thursday] unless thursday.nil?
  end
  
  def friday_conditions
    ["groups.friday = ?", friday] unless friday.nil?
  end
  
  def saturday_conditions
    ["groups.saturday = ?", saturday] unless saturday.nil?
  end
  
  def sunday_conditions
    ["groups.sunday = ?", sunday] unless sunday.nil?
  end
  
  def conditions
    [conditions_clauses.join(' AND '), *conditions_options]
  end
  
  def conditions_clauses
    conditions_parts.map { |condition| condition.first }
  end
  
  def conditions_options
    conditions_parts.map { |condition| condition[1..-1] }.flatten
  end
  
  def conditions_parts
    private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
  end
end
