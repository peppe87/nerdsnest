# == Schema Information
#
# Table name: groupships
#
#  id         :integer         not null, primary key
#  group_id   :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Groupship < ActiveRecord::Base
  attr_accessible :group_id, :user_id
  
  belongs_to :group
  belongs_to :user
  
  validates :user_id, presence: true
  validates :group_id, presence: true
  
end

