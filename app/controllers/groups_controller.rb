class GroupsController < ApplicationController
  
  def new
    @group = Group.new
  end

  def show
    @group = Group.find(params[:id]) #params is used to get the group id; it is equivalent to Group.find(1)
    @microposts = @group.microposts.paginate(page: params[:page])
    @micropost  = @group.microposts.build
  end

  def index
    # get all the groups, a page at time
    @groups = Group.paginate(page: params[:page])
  end

  def create
    @group = Group.new(params[:group])
    @group.master_id = current_user.id
    
    if @group.save
      @groupship = Groupship.new
      @groupship.user_id=current_user.id
      @groupship.group_id=@group.id
      @groupship.save
      if @group.play_online==true
        api_key = "16602591"
        api_secret = "3764408479f1902c9ad2607e2945e964f5cb12aa"
        if @opentok.nil?
          @opentok = OpenTok::OpenTokSDK.new api_key, api_secret
        end
        session = @opentok.create_session request.remote_addr
        @new_room = Room.new
        @new_room.sessionId= session.session_id
        @new_room.id=@group.id
        @new_room.name=@group.name
        @new_room.public=true
        @new_room.save
      end
        # Handle a successful save
      flash[:success] = "Group created!"
      redirect_to @group
    else
      render 'new'
    end
  end
  
  def edit
    @group = Group.find(params[:id])
  end
  
  def update
    @group = Group.find(params[:id])
     if @group.update_attributes(params[:group])
      # Handle a successful update.
      flash[:success] = "Informations updated"
      redirect_to @group
    else
      redirect_to @group
    end
  end
  
  def destroy
    @user = current_user
    @group = Group.find(params[:id])
    current_user.delete!(@group)
    flash[:success] = "Group deleted!"
    redirect_to @user
  end
  
  
  def advanced_search
    
  end
  
end
