class PagesController < ApplicationController
  
  def home
    if signed_in?
      @users = User.paginate(page: params[:page])
      @groups = Group.paginate(page: params[:page])
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def contact
  end
  
  def about
  end
  
  def help
  end
  
  def simple_search
    @groups = Group.search(params[:simple_search]).paginate(:page => params[:page])
    @users = User.search(params[:simple_search]).paginate(:page => params[:page])
    #Cerca attraverso un parametro ed impagina a seconda della corrispondenza
    @pages=@groups
  end

end
