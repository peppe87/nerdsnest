class MicropostsController < ApplicationController
  before_filter :signed_in_user, only: [:create, :destroy]
  before_filter :correct_user,   only: :destroy

  def create
    @micropost = Micropost.new
    @micropost.user_id = params[:micropost][:user_id]
    @micropost.sender_id = params[:micropost][:sender_id]
    @micropost.group_id = params[:micropost][:group_id]
    @micropost.content = params[:micropost][:content]
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to(:back)
    else
      @feed_items = []
      flash[:error] = "Error while posting your message"
      redirect_to current_user
    end
  end

  def destroy
    @micropost.destroy
    redirect_to root_path
  end

  private

    def correct_user
      @micropost = current_user.microposts.find_by_id(params[:id])
      redirect_to root_path if @micropost.nil?
    end
end