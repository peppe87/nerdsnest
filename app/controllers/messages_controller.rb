class MessagesController < ApplicationController
  
  
  def index
    if params[:mailbox] == "sent"
      @messages = current_user.sent_messages
    else
      @messages = current_user.received_messages
    end
  end
  
  def show
    @message = Message.read_message(params[:id], current_user)
  end
  
  def new
    @message = Message.new
    if params[:reply_to]
      @reply_to = current_user.received_messages.find(params[:reply_to])
      unless @reply_to.nil?
        @message.to = @reply_to.sender
        @message.subject = "Re: #{@reply_to.subject}"
        @message.body = "\n\nOriginal message:'\n\n#{@reply_to.body}'"
        @user=@message.to
      end
    
    else
     @user = User.find(params[:id])
     end
  end
  
  def create
    @message = Message.new
    @message.sender = User.find_by_name(params[:message][:sender])
    @message.recipient = User.find_by_name(params[:message][:recipient])
    @message.subject = params[:message][:subject]
    @message.body = params[:message][:body]

    if @message.save
      flash[:success] = "Message sent"
      redirect_to current_user
    else
      render :action => :new
    end
  end
  
  def delete_selected
    if request.post?
      if params[:delete]
        params[:delete].each { |id|
          @message = Message.find(:first, :conditions => ["messages.id = ? AND (sender_id = ? OR recipient_id = ?)", id, current_user, current_user])
          @message.mark_deleted(current_user) unless @message.nil?
        }
        flash[:notice] = "Messages deleted"
      end
      redirect_to :back
    end
  end
  
  private
    def set_user
        @user = User.find(params[:id])
    end
end