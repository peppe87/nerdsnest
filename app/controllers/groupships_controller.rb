class GroupshipsController < ApplicationController
  before_filter :signed_in_user
  
  def create
    @group = Group.find(params[:groupship][:group_id])
    current_user.join!(@group)
    flash[:success] = "You've joined this group!"
    redirect_to @group
  end
  
  def destroy
    @user = current_user
    @group = Groupship.find(params[:id]).group
    current_user.leave!(@group)
    flash[:success] = "You've left this group!"
    redirect_to @user
  end
end
