class ApplicationController < ActionController::Base
  protect_from_forgery
  # session helper per tutti i controller, serve per il login
  include SessionsHelper
end
